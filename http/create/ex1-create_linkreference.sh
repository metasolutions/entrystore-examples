#!/bin/sh

. ../config.sh
. ../init.sh

#Sign in
echoeval "curl '${BASE}auth/cookie'
     -c cookie_file
     -H 'Accept: application/json'
     --data-binary 'auth_username=${USERNAME}&auth_password=${PASSWORD}&auth_maxage=604800'" 1

#Data containing additional metadata for entry with entryid "specific"
RESURI="${BASE}${CONTEXT}/resource/specific"
ENCRE=$(encodeURIComponent "$RESURI")
ENCMD=$(encodeURIComponent "${BASE}${CONTEXT}/metadata/specific")
DCT="http://purl.org/dc/terms/"
DATA='{
      "metadata":{
        "'$RESURI'":{
           "'${DCT}'description": [{"type":"literal","value":"Exquisite banana","lang":"en"}]
          }
        }
      }'

#Create link-entry for URL http://entrystore.org
echoeval "curl '${BASE}${CONTEXT}?resource=${ENCRE}&cached-external-metadata=${ENCMD}&entrytype=linkreference'
         -b cookie_file
         -H 'Content-Type: application/json; charset=UTF-8'
         -H 'Accept: application/json'
         --data-binary '$DATA'"