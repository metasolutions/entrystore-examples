#!/bin/sh

. ../config.sh
. ../init.sh

#Sign in
echoeval "curl '${BASE}auth/cookie'
     -c cookie_file
     -H 'Accept: application/json'
     --data-binary 'auth_username=${USERNAME}&auth_password=${PASSWORD}&auth_maxage=604800'" 1

#Data containing metadata for URL http://entrystore.org
DATA='{
      "metadata":{
        "http://entrystore.org":{
           "http://purl.org/dc/terms/title":[{"type":"literal","value":"EntryStore website"}]
        }
    }'

#Create link-entry for URL http://entrystore.org
echoeval "curl '${BASE}${CONTEXT}?resource=http%3A%2F%2Fentrystore.org&entrytype=link'
         -b cookie_file
         -H 'Content-Type: application/json; charset=UTF-8'
         -H 'Accept: application/json'
         --data-binary '$DATA'"