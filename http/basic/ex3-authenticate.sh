#!/bin/bash

. ../config.sh
. ../init.sh

echoeval "curl '${BASE}auth/cookie'
     -H 'Accept: application/json'
     --data-binary 'auth_username=${USERNAME}&auth_password=${PASSWORD}&auth_maxage=604800'" 1