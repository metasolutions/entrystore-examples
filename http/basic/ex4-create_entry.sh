#!/bin/sh

. ../config.sh
. ../init.sh

#Sign in
echoeval "curl '${BASE}auth/cookie'
     -c cookie_file
     -H 'Accept: application/json'
     --data-binary 'auth_username=${USERNAME}&auth_password=${PASSWORD}&auth_maxage=604800'" 1

#Data containing metadata for new entry
DATA='{
      "metadata":{
        "'${BASE}${CONTEXT}'/resource/_newId": {
          "http://purl.org/dc/terms/title": [{"type":"literal","value":"Apple"}]
        }
      }
    }'

#Create entry
echoeval "curl '${BASE}${CONTEXT}'
         -b cookie_file
         -H 'Content-Type: application/json; charset=UTF-8'
         -H 'Accept: application/json'
         --data-binary '$DATA'"