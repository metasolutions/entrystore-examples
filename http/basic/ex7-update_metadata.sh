#!/bin/sh

. ../config.sh
. ../init.sh

#Sign in
echoeval "curl '${BASE}auth/cookie'
     -c cookie_file
     -H 'Accept: application/json'
     --data-binary 'auth_username=${USERNAME}&auth_password=${PASSWORD}&auth_maxage=604800'" 1

#New metadata
S="${BASE}${CONTEXT}/resource/specific"
DCT="http://purl.org/dc/terms/"
MD='{
      "'$S'":{
        "'${DCT}'relation": [{"type":"uri","value":"http://entrystore.org"}],
        "'${DCT}'description": [{"type":"literal","value":"A description","lang":"en"}],
        "'${DCT}'title": [{"type":"literal","value":"New title","lang":"en"}]
      }
    }'

#Update metadata for known entry 'specific'
echoeval "curl '${BASE}${CONTEXT}/metadata/specific'
         -X PUT
         -b cookie_file
         -H 'Content-Type: application/json; charset=UTF-8'
         -H 'Accept: application/json'
         --data-binary '$MD'" 1