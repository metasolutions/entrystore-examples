#!/bin/sh

. ../config.sh
. ../init.sh

#Sign in
echoeval "curl '${BASE}auth/cookie'
     -c cookie_file
     -H 'Accept: application/json'
     --data-binary 'auth_username=${USERNAME}&auth_password=${PASSWORD}&auth_maxage=604800'" 1

# Abbreviations for improved readability
MURI="${BASE}${CONTEXT}/metadata/specific"
STORE="http://entrystore.org/terms/"
GUEST="${BASE}_principals/entry/_guest"

# We can leave out server maintained triples such as:
# all relations between URIs for entry, metadata, resource and relations as well as
# dct:creator, dct:contributor, dct:created and dct:modified
INFO='{
    "'${MURI}'":{
      "'${STORE}'read":[{"type":"uri","value":"'${GUEST}'"}]
    }
  }'

#Update entryinformation with new ACL for known entry 'specific'
echoeval "curl '${BASE}${CONTEXT}/entry/specific'
         -X PUT
         -b cookie_file
         -H 'Content-Type: application/json; charset=UTF-8'
         -H 'Accept: application/json'
         --data-binary '$INFO'" 1