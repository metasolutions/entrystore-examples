#!/bin/sh

. ../config.sh
. ../init.sh

#Sign in
echoeval "curl '${BASE}auth/cookie'
     -c cookie_file
     -H 'Accept: application/json'
     --data-binary 'auth_username=${USERNAME}&auth_password=${PASSWORD}&auth_maxage=604800'" 1

#Data containing metadata for new entry
DATA='{
      "metadata":{
        "'${BASE}${CONTEXT}'/resource/_newId": {
          "http://purl.org/dc/terms/title": [{"type":"literal","value":"Apple"}]
        }
      }
    }'

#Create entry
echoeval "curl '${BASE}${CONTEXT}'
         -b cookie_file
         -o output
         -H 'Content-Type: application/json; charset=UTF-8'
         -H 'Accept: application/json'
         --data-binary '$DATA'" 1

#Extract the newly created entry id from the output file that contains
# something like {"entryId":55} where we want to extract "55"
EID=`cat output`
EID=${EID#*:} #Remove everything up to a colon
EID=${EID%\}} #Remove the } at the end

#Upload the file ../test.jpg to the newly created entry
echoeval "curl '${BASE}${CONTEXT}/resource/${EID}'
         -X PUT
         -b cookie_file
         -H 'Content-Type: image/jpg; charset=UTF-8'
         -H 'Accept: application/json'
         --data-binary '@../test.jpg'" 1