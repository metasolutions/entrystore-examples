echo $BASH_SOURCE

encodeURIComponent() {
  awk 'BEGIN {while (y++ < 125) z[sprintf("%c", y)] = y
  while (y = substr(ARGV[1], ++j, 1))
  q = y ~ /[[:alnum:]_.!~*\47()-]/ ? q y : q sprintf("%%%02X", z[y])
  print q}' "$1"
}

echoeval() {
  printf "==========\n"
  echo $1
  echo "----------"
  eval $1
  if [ $# -ne 1 ]; then
    if [ $? -eq 0 ]; then
      printf "\n--> Command successful <--"
    else
      printf "\n --> Command failed <--"
    fi
  fi
  printf "\n==========\n"
}

# URL escaped base
EBASE=$(encodeURIComponent "$BASE")

# Special escape for solr searches.
# First escape character ":" as "\:" then urlescape everything
SBASE=$(encodeURIComponent "${BASE//:/\\:}")
