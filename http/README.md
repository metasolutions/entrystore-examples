# HTTP requests

Below we outline the requests and responses. For the first three we outline the requests in 
both plain HTTP and also corresponding calls using the common command `curl`. For example 4-9 we 
only provide the requests in curl.

All examples below have a executable bash script that you can use. For these to work 
appropriately you have to provide a file `config.sh`, you can make a copy of `config.sh_example` 
and adapt it.

In the examples below we are assuming all requests are made using the following configuration:

1. Repository: `https://demo.entryscape.com/store`
2. Context: `128`
3. User: `test`
4. Password: `testtest`

For example 1, 2, 7, 8 and 9 to work an entry with a given id (arbitrarily 
chosen to be `specific`) needs to be present. Run the script `setup.sh` to make sure this entry is available. 
After running example 9 the special entry is removed and you need to run `setup.sh` again.

Note that in the examples below we have formated the curl requests for increased readability by
having flags on separate lines (using the common `\` character that is understood by bash). Furthermore, json expressions are 
formated into separate lines by allowing linebreaks inside of strings expressed by the surrounding quote character `'` .

## 1. Search for entries with a specific title
**Request plain HTTP**

    GET /store/search?type=solr&query=title:banana+AND+context:https%5C%3A%2F%2Fdemo.entryscape.com%2Fstore%2F128 HTTP/1.1
    Host: demo.entryscape.com

**Request using Curl:**

    curl 'https://demo.entryscape.com/store/search?type=solr&query=title:banana+AND+context:https%5C%3A%2F%2Fdemo.entryscape.com%2Fstore%2F128'
    
**Response:**

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Content-Length: 486
    
    {
      "offset": 0,
      "resource": {"children": [{
        "metadata": {"https://demo.entryscape.com/store/128/resource/43": {"http://purl.org/dc/terms/title": [{
          "type": "literal",
          "value": "Tasty banana"
        }]}},
        "rights": ["administer"],
        "contextId": "128",
        "relations": {},
        "entryId": "43",
        "info": {...} // Omitted for brevity
      }]},
      "rights": [
        "readmetadata",
        "readresource"
      ],
      "limit": 50,
      "results": 1
    }

## 2. Retrieve an entry with a specific identifier
**Request plain HTTP:**

    GET /store/128/entry/specific?includeAll HTTP/1.1
    Host: demo.entryscape.com
    Accept: application/json

**Request using Curl:**

    curl 'https://demo.entryscape.com/store/128/entry/specific?includeAll' \
      -H 'Accept:application/json'

**Response**

    HTTP/1.1 200 OK
    Content-Type: application/json;charset=UTF-8
    Content-Length: 431
    
    {
      "metadata": {"https://demo.entryscape.com/store/128/resource/specific": {"http://purl.org/dc/terms/title": [{
        "type": "literal",
        "value": "Entry with specific id"
      }]}},
      "rights": ["administer"],
      "relations": {},
      "entryId": "specific",
      "info": {...} // Omitted for brevity
      }
    }

## 3. Authenticate with username and password
**Request plain HTTP:**

    POST /store/auth/cookie HTTP/1.1
    Host: demo.entryscape.com
    Content-Length: 60
    
    auth_username=test&auth_password=testtest&auth_maxage=604800

**Request using Curl:**

    curl 'https://demo.entryscape.com/store/auth/cookie' \
      -H 'Accept: application/json' \
      --data-binary 'auth_username=test&auth_password=testtest&auth_maxage=604800'

**Response:**

    HTTP/1.1 204 No Content

## 4. Create an entry with metadata (RDF graph)
**Request using Curl:**

    curl 'https://demo.entryscape.com/store/auth/cookie' \
      -c cookie_file \
      -H 'Accept: application/json' \
      --data-binary 'auth_username=test&auth_password=testtest&auth_maxage=604800'
    ----
    curl 'https://demo.entryscape.com/store/128' \
      -b cookie_file 
      -H 'Content-Type: application/json; charset=UTF-8' \
      -H 'Accept: application/json' \
      --data-binary '{ "metadata":{
        "https://demo.entryscape.com/store/128/resource/_newId": {
          "http://purl.org/dc/terms/title": [{"type":"literal","value":"Apple"}]
        }
      }}'

**Response:**

    HTTP/1.1 204 No Content
    ---
    HTTP/1.1 201 Created
    Location:https://demo.entryscape.com/store/128/entry/64
    Content-Type: application/json;charset=UTF-8
    Content-Length: 14

    {"entryId":63}

## 5. Create an entry with a given URI (a link)
**Request using Curl:**

    curl 'https://demo.entryscape.com/store/auth/cookie' \
      -c cookie_file \
      -H 'Accept: application/json' \
      --data-binary 'auth_username=test&auth_password=testtest&auth_maxage=604800'
    ---
    curl 'https://demo.entryscape.com/store/128?resource=http%3A%2F%2Fentrystore.org&entrytype=link' \
    -b cookie_file \
    -H 'Content-Type: application/json; charset=UTF-8' \
    -H 'Accept: application/json' \
    --data-binary '{ "metadata":{ 
      "http://entrystore.org":{ 
        "http://purl.org/dc/terms/title":[{"type":"literal","value":"EntryStore website"}]
      }}'

**Response:**

    HTTP/1.1 204 No Content
    ---
    HTTP/1.1 201 Created
    Location:https://demo.entryscape.com/store/128/entry/64
    Content-Type: application/json;charset=UTF-8
    Content-Length: 14

    {"entryId":64}

## 6. Create an entry with a file (information resource)
**Request using Curl:**

    curl 'https://demo.entryscape.com/store/auth/cookie' \
      -c cookie_file \
      -H 'Accept: application/json' \
      --data-binary 'auth_username=test&auth_password=testtest&auth_maxage=604800'
    ---
    curl 'https://demo.entryscape.com/store/128' \
      -b cookie_file \
      -o output \
      -H 'Content-Type: application/json; charset=UTF-8' \
      -H 'Accept: application/json' \
      --data-binary '{ "metadata":{ 
        "https://demo.entryscape.com/store/128/resource/_newId": {
          "http://purl.org/dc/terms/title": [{"type":"literal","value":"Entry with a file"}]
        }
      }}'
    ---
    curl 'https://demo.entryscape.com/store/128/resource/69' \
      -X PUT \
      -b cookie_file \
      -H 'Content-Type: image/jpg; charset=UTF-8' \
      -H 'Accept: application/json' \
      --data-binary '@../test.jpg'

**Response:**

    HTTP/1.1 204 No Content
    ---
    HTTP/1.1 201 Created
    Location:https://demo.entryscape.com/store/128/entry/69
    Content-Type: application/json;charset=UTF-8
    Content-Length: 14

    {"entryId":69}
    ---
    HTTP/1.1 201 Created
    Content-Length:64
    Content-Type:application/json;charset=UTF-8
    
    {"success":"The file is uploaded", "format": "image/jpg"}


## 7. Update metadata for an entry

**Request using Curl:**

    curl 'https://demo.entryscape.com/store/auth/cookie' -c cookie_file -H 'Accept: application/json' --data-binary 'auth_username=test&auth_password=testtest&auth_maxage=604800'
    ---
    curl 'https://demo.entryscape.com/store/128/metadata/specific' \
      -X PUT -b cookie_file \
      -H 'Content-Type: application/json; charset=UTF-8' \
      -H 'Accept: application/json' \
      --data-binary '{
        "https://demo.entryscape.com/store/128/resource/specific":{ 
          "http://purl.org/dc/terms/relation": [{"type":"uri","value":"http://entrystore.org"}],
          "http://purl.org/dc/terms/description": [{"type":"literal","value":"A description","lang":"en"}],
          "http://purl.org/dc/terms/title": [{"type":"literal","value":"New title","lang":"en"}]
        }
      }'
**Response:**

    HTTP/1.1 204 No Content
    ---
    HTTP/1.1 204 No Content

## 8. Publish an entry (change ACL)

**Request using Curl:**

    curl 'https://demo.entryscape.com/store/auth/cookie' -c cookie_file -H 'Accept: application/json' --data-binary 'auth_username=test&auth_password=testtest&auth_maxage=604800'
    ---
    curl 'https://demo.entryscape.com/store/128/entry/specific' \
      -X PUT \
      -b cookie_file \
      -H 'Content-Type: application/json; charset=UTF-8' \
      -H 'Accept: application/json' \
      --data-binary '{ "https://demo.entryscape.com/store/128/metadata/specific":{ 
        "http://entrystore.org/terms/read":[
          {"type":"uri","value":"https://demo.entryscape.com/store/_principals/entry/_guest"}
        ]}}'
    
**Response:**

    HTTP/1.1 204 No Content
    ---
    HTTP/1.1 204 No Content

## 9. Delete an entry

**Request using Curl:**

    curl 'https://demo.entryscape.com/store/auth/cookie' -c cookie_file -H 'Accept: application/json' --data-binary 'auth_username=test&auth_password=testtest&auth_maxage=604800'
    ---
    curl 'https://demo.entryscape.com/store/128/entry/specific' \
      -X DELETE
      -b cookie_file \
    
**Response:**

    HTTP/1.1 204 No Content
    ---
    HTTP/1.1 204 No Content
