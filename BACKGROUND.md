# Metadata in RDF
It is often the case that you need to store information about entities regardless whether they are digital files, webpages, physical persons or identifiable data-structures.
 This information is often referred to as data, but sometimes the entities themselves are considered data (e.g. if the entities are files or identifiable data-structures) and then we instead talk about the information as metadata.

Whenever we have information that *is about* an identifiable entity, RDF is a good choice. RDF (Resource Description Framework) is a W3C recommendation that defines a language for providing information about identifiable entities referred to as *resources*. The language provides statements (facts) about resources that can also take the form of relations between resources. If several statements are bundled togehter they form a graph that can be provided online in an adressable manner. Together such graphs forms a web of data, also referred to as linked data.

# Entries in EntryStore
EntryStore provides a REST API for manipulating RDF together with the resources they describe with the help of what is referred to as an `entry`. An entry is a way to manage a chunk of metadata focused on a single resource and sometimes the resource itself. Examples when the resource is not part of the entry is when it is already provided elsewhere on the web or when it has no digital representation (e.g. a physical person). The entry also contains specific `entry information`, that keeps track of things like whether the resource is part of the entry, who has access to the metadata and the resource, when and by who the entry was modified etc.

To summarize an entry contains:

+ Resource - can take the following forms:
  + uploaded file
  + entrystore resource (graph / string / list / user / group / context)
  + identifier (e.g. representing a physical object)
  + link (e.g. a web address you want to describe)
+ Metadata - RDF expression
+ External metadata - metadata fetched from other system
+ Entry information - administrative information like:
  + access controll
  + creation and modification dates
  + creators / contributors
  + information about metadata versions