﻿using System;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Exceptions;
using EntryStore.Models;
using VDS.RDF;

namespace ExampleBasic08
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 8 - Publish an entry (change ACL)");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var user = new User
            {
                Username = "admin",
                Password = "adminadmin"
            };
            var contextId = "1";
            var entryId = "specific";

            try
            {
                // Login
                await client.LoginAsync(user);

                // Create RDF graph, set read permission
                var acl = new Graph();
                var metadataUri = client.CreateMetadataUri(contextId, entryId);
                var subjectNode = acl.CreateUriNode(metadataUri);
                var readPredicateNode = acl.CreateUriNode(new Uri("http://entrystore.org/terms/read"));
                var guestUri = client.CreateResourceUri("_principals", "_guest");
                var readObjectNode = acl.CreateUriNode(guestUri);
                acl.Assert(new Triple(subjectNode, readPredicateNode, readObjectNode));

                // Update entry in entrystore
                await client.UpdateEntryAccessAsync(contextId, entryId, acl);

                Console.WriteLine("Access was updated with new ACL");
            }
            catch (UnauthorizedException)
            {
                Console.WriteLine("Login failed");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
