﻿using System;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Models;

namespace ExampleBasic01
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 1 - Search for entries with a specific title");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var query = new SearchOptions
            {
                Title = "banana",
                ContextId = "1"
            };
            var searchResult = await client.SearchAsync(query);

            foreach (var entry in searchResult.Entries)
            {
                Console.WriteLine($"Found entry '{entry.EntryId}' that has a title including 'banana'");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
