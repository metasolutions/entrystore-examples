﻿using System;
using System.IO;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Exceptions;
using EntryStore.Models;
using VDS.RDF;

namespace ExampleBasic07
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 7 - Update metadata for an entry");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var user = new User
            {
                Username = "admin",
                Password = "adminadmin"
            };
            var contextId = "1";
            var entryId = "specific";

            try
            {
                // Login
                await client.LoginAsync(user);

                // Create RDF graph containing subject
                var metadata = new Graph();
                var entryUri = client.CreateResourceUri(contextId, entryId);
                var subjectNode = metadata.CreateUriNode(entryUri);

                // Relation
                var relationPredicateNode = metadata.CreateUriNode(new Uri("http://purl.org/dc/terms/relation"));
                var relationObjectNode = metadata.CreateUriNode(new Uri("http://entrystore.org"));
                metadata.Assert(new Triple(subjectNode, relationPredicateNode, relationObjectNode));

                // Description
                var descriptionPredicateNode = metadata.CreateUriNode(new Uri("http://purl.org/dc/terms/description"));
                var descriptionObjectNode = metadata.CreateLiteralNode("A description", "en");
                metadata.Assert(new Triple(subjectNode, descriptionPredicateNode, descriptionObjectNode));

                // Title
                var titlePredicateNode = metadata.CreateUriNode(new Uri("http://purl.org/dc/terms/title"));
                var titleObjectNode = metadata.CreateLiteralNode("New title", "en");
                metadata.Assert(new Triple(subjectNode, titlePredicateNode, titleObjectNode));

                // Update entry in entrystore
                await client.UpdateEntryMetadataAsync(contextId, entryId, metadata);
                
                Console.WriteLine("Entry was updated with new metadata");
            }
            catch (UnauthorizedException)
            {
                Console.WriteLine("Login failed");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
