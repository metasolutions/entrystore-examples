﻿using System;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Exceptions;
using EntryStore.Models;
using VDS.RDF;

namespace ExampleBasic04
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 4 - Create an entry with metadata (RDF graph)");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var user = new User
            {
                Username = "admin",
                Password = "adminadmin"
            };
            var contextId = "1";

            try
            {
                // Login
                await client.LoginAsync(user);

                // Create RDF graph, assert that the entry has the title: 'Apple'
                var metadata = new Graph();
                var newEntryUri = client.CreateResourceUri(contextId);
                var subjectNode = metadata.CreateUriNode(newEntryUri);
                var predicateNode = metadata.CreateUriNode(new Uri("http://purl.org/dc/terms/title"));
                var objectNode = metadata.CreateLiteralNode("Apple");
                metadata.Assert(new Triple(subjectNode, predicateNode, objectNode));

                // Create entry in entrystore
                var newEntry = new NewEntry
                {
                    ContextId = contextId,
                    Metadata = metadata
                };
                var createdItem = await client.CreateEntryAsync(newEntry);
                Console.WriteLine($"Entry with metadata id: {createdItem.EntryId} created");
            }
            catch (UnauthorizedException)
            {
                Console.WriteLine("Login failed");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
