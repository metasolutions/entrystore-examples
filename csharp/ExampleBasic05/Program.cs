﻿using System;
using System.Threading.Tasks;
using System.Web.UI;
using EntryStore;
using EntryStore.Exceptions;
using EntryStore.Models;
using VDS.RDF;

namespace ExampleBasic05
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 5 - Create an entry with a given URI (a link)");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var user = new User
            {
                Username = "admin",
                Password = "adminadmin"
            };
            var contextId = "1";

            try
            {
                // Login
                await client.LoginAsync(user);

                // Create RDF graph, assert that the website has the title: 'EntryStore website'
                var metadata = new Graph();
                var subjectNode = metadata.CreateUriNode(new Uri("http://entrystore.org"));
                var predicateNode = metadata.CreateUriNode(new Uri("http://purl.org/dc/terms/title"));
                var objectNode = metadata.CreateLiteralNode("EntryStore website");
                metadata.Assert(new Triple(subjectNode, predicateNode, objectNode));

                // Create entry in entrystore
                var newEntry = new NewEntry
                {
                    ContextId = contextId,
                    Metadata = metadata,
                    Resource = "http://entrystore.org",
                    EntryType = EntryType.Link
                };
                var createdItem = await client.CreateEntryAsync(newEntry);
                Console.WriteLine($"Entry with metadata id: {createdItem.EntryId} created");
            }
            catch (UnauthorizedException)
            {
                Console.WriteLine("Login failed");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
