﻿using System;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Exceptions;
using EntryStore.Models;
using VDS.RDF;

namespace ExampleBasic09
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 9 - Delete an entry");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var user = new User
            {
                Username = "admin",
                Password = "adminadmin"
            };
            var contextId = "1";
            var entryId = "specific";

            try
            {
                // Login
                await client.LoginAsync(user);

                // Delete entry in entrystore
                await client.DeleteEntryAsync(contextId, entryId);

                Console.WriteLine("Entry was deleted successfully");
            }
            catch (UnauthorizedException)
            {
                Console.WriteLine("Login failed");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
