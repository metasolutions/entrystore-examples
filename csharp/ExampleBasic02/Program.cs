﻿using System;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Models;

namespace ExampleBasic02
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 2 - Retrieve an entry with a specific identifier");
            var contextId = "1";
            var entryId = "specific";

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var entry = await client.GetEntryAsync(contextId, entryId);

            if (entry != null)
            {
                Console.WriteLine($"Loaded entry with id '{entry.EntryId}'");
            }
            else
            {
                Console.WriteLine("No entry found");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
