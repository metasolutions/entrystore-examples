﻿using System;
using System.IO;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Exceptions;
using EntryStore.Models;
using VDS.RDF;

namespace ExampleBasic06
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 6 - Create an entry with a file (information resource)");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var user = new User
            {
                Username = "admin",
                Password = "adminadmin"
            };
            var contextId = "1";

            try
            {
                // Login
                await client.LoginAsync(user);

                // Create RDF graph, assert that the entry has the title: 'Entry with a file'
                var metadata = new Graph();
                var newEntryUri = client.CreateResourceUri(contextId);
                var subjectNode = metadata.CreateUriNode(newEntryUri);
                var predicateNode = metadata.CreateUriNode(new Uri("http://purl.org/dc/terms/title"));
                var objectNode = metadata.CreateLiteralNode("Entry with a file");
                metadata.Assert(new Triple(subjectNode, predicateNode, objectNode));

                // Create entry in entrystore
                var newEntry = new NewEntry
                {
                    ContextId = contextId,
                    Metadata = metadata
                };
                var createdItem = await client.CreateEntryAsync(newEntry);

                // Read file to memory and upload
                using (var fileStream = File.OpenRead("./test.jpg"))
                {
                    var uploadResult = await client.UploadFileAsync(new UploadFileRequest
                    {
                        ContextId = contextId,
                        EntryId = createdItem.EntryId,
                        Filename = "test.jpg",
                        DataStream = fileStream,
                        ContentType = "image/jpg"
                    });
                    Console.WriteLine($"File was uploaded to newly created entryId: {createdItem.EntryId} with format: {uploadResult.Format}");
                }
            }
            catch (UnauthorizedException)
            {
                Console.WriteLine("Login failed");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
