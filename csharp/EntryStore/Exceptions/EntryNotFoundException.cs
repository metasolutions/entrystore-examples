﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EntryStore.Exceptions
{
    public class EntryNotFoundException : Exception
    {
        public EntryNotFoundException()
        {
        }

        public EntryNotFoundException(string message)
            : base(message)
        {
        }
    }
}
