﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VDS.RDF;
using VDS.RDF.Parsing;
using VDS.RDF.Writing;

namespace EntryStore
{
    internal static class Extensions
    {
        /// <summary>
        /// Converts JSON object to RDF graph
        /// </summary>
        /// <param name="jsonObject"></param>
        /// <returns></returns>
        public static Graph ToRdf(this JObject jsonObject)
        {
            if (jsonObject == null)
            {
                return null;
            }

            // Convert JSON object to JSON string
            var jsonString = JsonConvert.SerializeObject(jsonObject);

            // Convert JSON string to RDF Graph
            var rdf = new Graph();
            rdf.LoadFromString(jsonString, new RdfJsonParser());
            return rdf;
        }

        /// <summary>
        /// Converts RDF graph to JSON object
        /// </summary>
        /// <param name="rdf"></param>
        /// <returns></returns>
        public static JObject ToJson(this Graph rdf)
        {
            if (rdf == null)
            {
                return null;
            }

            // Convert RDF graph to JSON string
            var jsonWriter = new RdfJsonWriter { PrettyPrintMode = false };
            var jsonString = StringWriter.Write(rdf, jsonWriter);

            // Convert JSON string to JSON object
            var jsonObject = JObject.Parse(jsonString);
            return jsonObject;
        }
    }
}
