﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using EntryStore.Exceptions;
using EntryStore.Models;
using EntryStore.ResponseModels;
using RestSharp;
using VDS.RDF;

namespace EntryStore
{
    public class EntryStoreClient : IEntryStoreClient
    {
        private readonly RestClient _restClient;

        public EntryStoreClient(Uri baseUri)
        {
            // Ensure that baseUri ends with '/' 
            if (!baseUri.AbsoluteUri.EndsWith("/"))
            {
                baseUri = new Uri(baseUri.AbsoluteUri + "/");
            }
            _restClient = new RestClient(baseUri)
            {
                CookieContainer = new CookieContainer() // Used to store login
            };
            _restClient.AddHandler("application/json", new NewtonsoftJsonSerializer()); // Use Json.Net as default JSON deserializer
        }

        public Uri CreateResourceUri(string contextId, string entryId = "_newId")
        {
            return new Uri($"{_restClient.BaseUrl}{contextId}/resource/{entryId}");
        }

        public Uri CreateMetadataUri(string contextId, string entryId = "_newId")
        {
            return new Uri($"{_restClient.BaseUrl}{contextId}/metadata/{entryId}");
        }

        public Uri CreateEntryUri(string contextId, string entryId = "_newId")
        {
            return new Uri($"{_restClient.BaseUrl}{contextId}/entry/{entryId}");
        }

        public async Task<SearchResult> SearchAsync(SearchOptions searchOptions)
        {
            // Check for errors
            if (searchOptions == null)
            {
                throw new ArgumentException("searchOptions is required");
            }
            var validationErrors = searchOptions.GetValidationErrors().ToList();
            if (validationErrors.Count > 0)
            {
                throw new BadRequestException(string.Join(", ", validationErrors));
            }

            // Build request
            var request = new RestRequest("search", Method.GET);

            // Set solr parameter
            request.AddParameter("type", "solr");

            // Handle pagination
            if (searchOptions.Offset > 0)
            {
                request.AddParameter("offset", searchOptions.Offset);
            }

            // Build search query parameter
            var conditions = new List<string>();
            if (searchOptions.Title != null)
            {
                conditions.Add($"title:{searchOptions.Title}");
            }
            if (searchOptions.ContextId != null)
            {
                // Context id is relative to entry store base uri
                var contextUri = _restClient.BaseUrl + searchOptions.ContextId;

                // Escape ':' character, since it is used as query operator
                contextUri = contextUri.Replace(":", "\\:");

                conditions.Add($"context:{contextUri}");
            }
            request.AddParameter("query", string.Join(" AND ", conditions));

            // Run request
            var response = await _restClient.ExecuteTaskAsync<SearchResponse>(request);
            ThrowExceptionOnError(response);

            // Create result page
            var data = response.Data;
            return new SearchResult
            {
                Entries = data.Resource.Children.Select(x => x.ToRdfEntry()).ToList(),
                Limit = data.Limit,
                Offset = data.Offset,
                Results = data.Results
            };
        }
        
        public async Task<Entry> GetEntryAsync(string contextId, string entryId)
        {
            // Check for errors
            if (string.IsNullOrWhiteSpace(contextId))
            {
                throw new ArgumentException("Missing contextId");
            }
            if (string.IsNullOrWhiteSpace(entryId))
            {
                throw new ArgumentException("Missing contextId");
            }

            // Make query string
            var request = new RestRequest($"{contextId}/entry/{entryId}?includeAll", Method.GET);

            // Run request
            var response = await _restClient.ExecuteTaskAsync<EntryWithoutRdf>(request);
            if (response.StatusCode == HttpStatusCode.NotFound)
            {
                return null;
            }
            ThrowExceptionOnError(response);

            // Create result
            var entryWithoutRdf = response.Data;
            return entryWithoutRdf.ToRdfEntry();
        }
        
        public async Task LoginAsync(User user, int maxAgeSeconds = 604800)
        {
            // Check for errors
            if (user == null || string.IsNullOrWhiteSpace(user.Username) || string.IsNullOrWhiteSpace(user.Password))
            {
                throw new ArgumentException("username/password is required");
            }
            if (maxAgeSeconds <= 0)
            {
                throw new ArgumentException("Invalid maxAgeSeconds");
            }

            // Make request
            var request = new RestRequest("auth/cookie", Method.POST);
            request.AddParameter("auth_username", user.Username);
            request.AddParameter("auth_password", user.Password);
            request.AddParameter("auth_maxage", maxAgeSeconds);

            // Run request
            var response = await _restClient.ExecuteTaskAsync(request);
            ThrowExceptionOnError(response);
        }
        
        public async Task<CreatedEntry> CreateEntryAsync(NewEntry newEntry)
        {
            // Check for errors
            if (newEntry == null)
            {
                throw new ArgumentException("newEntry is required");
            }
            var validationErrors = newEntry.GetValidationErrors().ToList();
            if (validationErrors.Count > 0)
            {
                throw new BadRequestException(string.Join(", ", validationErrors));
            }

            // Make request
            var request = new RestRequest(newEntry.ContextId, Method.POST)
            {
                JsonSerializer = new NewtonsoftJsonSerializer(),
                RequestFormat = DataFormat.Json
            };

            if (!string.IsNullOrWhiteSpace(newEntry.Resource))
            {
                request.AddQueryParameter("resource", newEntry.Resource);
            }
            if (newEntry.EntryType != EntryType.Normal)
            {
                string entryType;
                switch (newEntry.EntryType)
                {
                    case EntryType.Link:
                        entryType = "link";
                        break;
                    default:
                        throw new ArgumentException($"Unhandled entryType: {(int)newEntry.EntryType}");
                }
                request.AddQueryParameter("entryType", entryType);
            }

            request.AddBody(EntryWithoutRdf.FromNewRdfEntry(newEntry));

            // Run request
            var response = await _restClient.ExecuteTaskAsync<CreatedEntry>(request);
            ThrowExceptionOnError(response);

            // Set contextid
            var createdEntry = response.Data;
            createdEntry.ContextId = newEntry.ContextId;

            // Create result
            return createdEntry;
        }
        
        public async Task<UploadResult> UploadFileAsync(UploadFileRequest uploadFileRequest)
        {
            // Check for errors
            if (uploadFileRequest == null)
            {
                throw new ArgumentException("uploadFileRequest is required");
            }
            var validationErrors = uploadFileRequest.GetValidationErrors().ToList();
            if (validationErrors.Count > 0)
            {
                throw new BadRequestException(string.Join(", ", validationErrors));
            }

            // Make request
            var request = new RestRequest($"{uploadFileRequest.ContextId}/resource/{uploadFileRequest.EntryId}", Method.PUT);
            var filename = !string.IsNullOrWhiteSpace(uploadFileRequest.Filename) ? uploadFileRequest.Filename : null;
            request.AddFile(filename, s => uploadFileRequest.DataStream.CopyTo(s), filename, uploadFileRequest.DataStream.Length, uploadFileRequest.ContentType);

            // Run request
            var response = await _restClient.ExecuteTaskAsync<UploadResult>(request);
            ThrowExceptionOnError(response);

            // Create result
            return response.Data;
        }
        
        public async Task UpdateEntryMetadataAsync(string contextId, string entryId, Graph metadata)
        {
            // Check for errors
            if (string.IsNullOrWhiteSpace(contextId))
            {
                throw new ArgumentException("Missing contextId");
            }
            if (string.IsNullOrWhiteSpace(entryId))
            {
                throw new ArgumentException("Missing contextId");
            }
            if (metadata == null)
            {
                throw new ArgumentException("Missing metadata");
            }

            // Make query string
            var request = new RestRequest($"{contextId}/metadata/{entryId}", Method.PUT)
            {
                JsonSerializer = new NewtonsoftJsonSerializer(),
                RequestFormat = DataFormat.Json
            };
            request.AddBody(metadata.ToJson());

            // Run request
            var response = await _restClient.ExecuteTaskAsync(request);
            ThrowExceptionOnError(response);
        }
        
        public async Task UpdateEntryAccessAsync(string contextId, string entryId, Graph acl)
        {
            // Check for errors
            if (string.IsNullOrWhiteSpace(contextId))
            {
                throw new ArgumentException("Missing contextId");
            }
            if (string.IsNullOrWhiteSpace(entryId))
            {
                throw new ArgumentException("Missing contextId");
            }
            if (acl == null)
            {
                throw new ArgumentException("Missing acl");
            }

            // Make query string
            var request = new RestRequest($"{contextId}/entry/{entryId}", Method.PUT)
            {
                JsonSerializer = new NewtonsoftJsonSerializer(),
                RequestFormat = DataFormat.Json
            };
            request.AddBody(acl.ToJson());

            // Run request
            var response = await _restClient.ExecuteTaskAsync(request);
            ThrowExceptionOnError(response);
        }
        
        public async Task DeleteEntryAsync(string contextId, string entryId)
        {
            // Check for errors
            if (string.IsNullOrWhiteSpace(contextId))
            {
                throw new ArgumentException("Missing contextId");
            }
            if (string.IsNullOrWhiteSpace(entryId))
            {
                throw new ArgumentException("Missing entryId");
            }

            // Make query string
            var request = new RestRequest($"{contextId}/entry/{entryId}", Method.DELETE);

            // Run request
            var response = await _restClient.ExecuteTaskAsync(request);
            ThrowExceptionOnError(response);
        }

        /// <summary>
        /// If the given response contains an error, throw an exception
        /// </summary>
        /// <param name="response"></param>
        private void ThrowExceptionOnError(IRestResponse response)
        {
            if (response.IsSuccessful) return;

            switch (response.StatusCode)
            {
                case HttpStatusCode.Unauthorized:
                    throw new UnauthorizedException();
                case HttpStatusCode.NotFound:
                    throw new EntryNotFoundException();
                default:
                    throw new RequestFailedException();
            }
        }
    }

    public interface IEntryStoreClient
    {
        /// <summary>
        /// TODO: Comment
        /// </summary>
        /// <param name="contextId"></param>
        /// <param name="entryId"></param>
        /// <returns></returns>
        Uri CreateResourceUri(string contextId, string entryId = "_newId");

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="contextId"></param>
        /// <param name="entryId"></param>
        /// <returns></returns>
        Uri CreateMetadataUri(string contextId, string entryId = "_newId");

        /// <summary>
        /// TODO
        /// </summary>
        /// <param name="contextId"></param>
        /// <param name="entryId"></param>
        /// <returns></returns>
        Uri CreateEntryUri(string contextId, string entryId = "_newId");

        /// <summary>
        /// Search asynchronously for entries that satisfy a number of conditions
        /// </summary>
        /// <param name="searchOptions">Conditions to set on the search query</param>
        /// <returns></returns>
        Task<SearchResult> SearchAsync(SearchOptions searchOptions);

        /// <summary>
        /// Retrieves a single entry from the repository by ID
        /// </summary>
        /// <param name="contextId">The context to load from</param>
        /// <param name="entryId">The id of the entry to load</param>
        /// <returns></returns>
        Task<Entry> GetEntryAsync(string contextId, string entryId);

        /// <summary>
        /// Login with this client. The session will be stored for future request until logging out.
        /// </summary>
        /// <param name="user">The user to log in with</param>
        /// <param name="maxAgeSeconds">The maximum session length in seconds. Default: 1 week</param>
        /// <returns></returns>
        Task LoginAsync(User user, int maxAgeSeconds = 604800);

        /// <summary>
        /// Creates a new entry with the given parameters
        /// </summary>
        /// <param name="newEntry">The new entry to create</param>
        /// <returns></returns>
        Task<CreatedEntry> CreateEntryAsync(NewEntry newEntry);

        /// <summary>
        /// Uploads a file to a given entry
        /// </summary>
        /// <param name="uploadFileRequest">Parameters to the request</param>
        /// <returns></returns>
        Task<UploadResult> UploadFileAsync(UploadFileRequest uploadFileRequest);

        /// <summary>
        /// Updates the metadata on a given entry
        /// </summary>
        /// <param name="contextId">The context containing the entry to update</param>
        /// <param name="entryId">The id of the entry to update</param>
        /// <param name="metadata">The new metadata to insert</param>
        Task UpdateEntryMetadataAsync(string contextId, string entryId, Graph metadata);

        /// <summary>
        /// Updates the ACL of the given entry
        /// </summary>
        /// <param name="contextId">The context containing the entry to update</param>
        /// <param name="entryId">The id of the entry to update</param>
        /// <param name="acl">The new access control list (ACL) to use</param>
        /// <returns></returns>
        Task UpdateEntryAccessAsync(string contextId, string entryId, Graph acl);

        /// <summary>
        /// Deletes an given entry
        /// </summary>
        /// <param name="contextId">The context containing the entry to delete</param>
        /// <param name="entryId">The id of the entry to delete</param>
        /// <returns></returns>
        Task DeleteEntryAsync(string contextId, string entryId);
    }
}
