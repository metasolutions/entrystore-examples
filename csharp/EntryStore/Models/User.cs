﻿namespace EntryStore.Models
{
    public class User
    {
        /// <summary>
        /// The username used for logging in to an EntryStore repository
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// The username used for logging in to an EntryStore repository
        /// </summary>
        public string Password { get; set; }
    }
}
