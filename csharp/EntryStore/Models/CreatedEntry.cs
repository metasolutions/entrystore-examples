﻿namespace EntryStore.Models
{
    public class CreatedEntry
    {
        /// <summary>
        /// The repository id of the newly created entry
        /// </summary>
        public string EntryId { get; set; }

        /// <summary>
        /// The context id containing the newly created entry
        /// </summary>
        public string ContextId { get; set; }
    }
}
