﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EntryStore.Models
{
    public class UploadFileRequest
    {
        /// <summary>
        /// TODO
        /// </summary>
        public string ContextId { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public string EntryId { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public Stream DataStream { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Validates this upload request, and returnes a list of errors if any
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetValidationErrors()
        {
            if (string.IsNullOrWhiteSpace(ContextId))
            {
                yield return "ContextId is required";
            }

            if (string.IsNullOrWhiteSpace(EntryId))
            {
                yield return "EntryId is required";
            }

            if (DataStream == null)
            {
                yield return "File is required";
            }

            if (string.IsNullOrWhiteSpace(ContentType))
            {
                yield return "ContentType is required";
            }
        }
    }
}
