﻿using System.Collections.Generic;
using VDS.RDF;

namespace EntryStore.Models
{
    public class Entry
    {
        /// <summary>
        /// TODO
        /// </summary>
        public Graph Metadata { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public Graph Info { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public List<string> Rights { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public string EntryId { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public Graph Relations { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public string ContextId { get; set; }
    }
}
