﻿using System.Collections.Generic;

namespace EntryStore.Models
{
    public class SearchResult
    {
        /// <summary>
        /// The entries that was returned in this result page
        /// </summary>
        public List<Entry> Entries { get; set; }

        /// <summary>
        /// The number of entries that the page is limited to
        /// </summary>
        public int Limit { get; set; }

        /// <summary>
        /// The offset of the current page
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// The total number of entries that 
        /// </summary>
        public int Results { get; set; }
    }
}
