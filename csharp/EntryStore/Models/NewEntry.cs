﻿using System.Collections.Generic;
using VDS.RDF;

namespace EntryStore.Models
{
    public class NewEntry
    {
        /// <summary>
        /// TODO
        /// </summary>
        public Graph Metadata { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public Graph Info { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public List<string> Rights { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public Graph Relations { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public string ContextId { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public string Resource { get; set; }

        /// <summary>
        /// TODO
        /// </summary>
        public EntryType EntryType { get; set; }

        /// <summary>
        /// Validates this new entry, and returnes a list of errors if any
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetValidationErrors()
        {
            if (string.IsNullOrWhiteSpace(ContextId))
            {
                yield return "ContextId is required";
            }
        }
    }
}
