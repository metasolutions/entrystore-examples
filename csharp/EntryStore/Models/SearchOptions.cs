﻿using System.Collections.Generic;

namespace EntryStore.Models
{
    public class SearchOptions
    {
        /// <summary>
        /// Used to search for entries that contain this string in title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Limits the entries to the specified context id within this repository. E.g: "128"
        /// </summary>
        public string ContextId { get; set; }

        /// <summary>
        /// Used to paginate the search response
        /// </summary>
        public int Offset { get; set; }

        /// <summary>
        /// Validates these query options, and returnes a list of errors if any
        /// </summary>
        /// <returns></returns>
        public IEnumerable<string> GetValidationErrors()
        {
            if (Title == null && ContextId == null)
            {
                yield return "At least one search parameter is required";
            }
        }
    }
}
