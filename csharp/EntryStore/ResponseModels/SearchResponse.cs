﻿namespace EntryStore.ResponseModels
{
    internal class SearchResponse
    {
        public int Offset { get; set; }
        public Resource Resource { get; set; }
        public string Rights { get; set; }
        public int Limit { get; set; }
        public int Results { get; set; }
    }
}
