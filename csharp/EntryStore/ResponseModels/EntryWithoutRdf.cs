﻿using System;
using System.Collections.Generic;
using EntryStore.Models;
using Newtonsoft.Json.Linq;

namespace EntryStore.ResponseModels
{
    /// <summary>
    /// A representation of an EntryStore-entry, where all RDF-properties are not parsed
    /// </summary>
    internal class EntryWithoutRdf
    {
        /// <summary>
        /// RDF
        /// </summary>
        public JObject Metadata { get; set; }

        /// <summary>
        /// RDF
        /// </summary>
        public JObject Info { get; set; }
        
        public List<string> Rights { get; set; }
        
        public string EntryId { get; set; }

        /// <summary>
        /// RDF
        /// </summary>
        public JObject Relations { get; set; }
        
        public string ContextId { get; set; }

        /// <summary>
        /// Converts this entry to a format where RDF properties are parsed
        /// </summary>
        /// <returns></returns>
        public Entry ToRdfEntry()
        {
            var entry = new Entry
            {
                Rights = Rights,
                EntryId = EntryId,
                ContextId = ContextId,
                Metadata = Metadata.ToRdf(),
                Info = Info.ToRdf(),
                Relations = Relations.ToRdf()
            };

            return entry;
        }

        public static EntryWithoutRdf FromNewRdfEntry(NewEntry entry)
        {
            if (entry == null)
            {
                throw new ArgumentNullException();
            }

            var entryWithoutRdf = new EntryWithoutRdf
            {
                Rights = entry.Rights,
                ContextId = entry.ContextId,
                Metadata = entry.Metadata.ToJson(),
                Info = entry.Info.ToJson(),
                Relations = entry.Relations.ToJson(),
            };
            return entryWithoutRdf;
        }
    }
}
