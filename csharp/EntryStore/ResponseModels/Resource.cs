﻿using System.Collections.Generic;

namespace EntryStore.ResponseModels
{
    internal class Resource
    {
        public List<EntryWithoutRdf> Children { get; set; }
    }
}