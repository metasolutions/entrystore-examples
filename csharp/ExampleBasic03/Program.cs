﻿using System;
using System.Threading.Tasks;
using EntryStore;
using EntryStore.Exceptions;
using EntryStore.Models;

namespace ExampleBasic03
{
    class Program
    {
        static void Main(string[] args)
        {
            RunAsync().Wait();
        }

        static async Task RunAsync()
        {
            Console.WriteLine("Running basic example 3 - Authenticate with username and password");

            IEntryStoreClient client = new EntryStoreClient(new Uri("http://127.0.0.1:8281/store/"));
            var user = new User
            {
                Username = "admin",
                Password = "adminadmin"
            };

            try
            {
                await client.LoginAsync(user);
                Console.WriteLine("Successfully logged in");
            }
            catch (UnauthorizedException)
            {
                Console.WriteLine("Login failed");
            }

            Console.WriteLine("");
            Console.WriteLine("Press any key to close");
            Console.Read();
        }
    }
}
