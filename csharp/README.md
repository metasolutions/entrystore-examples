# EntryStore C# library

Included in this solution are examples which use a common C# library for communicating with the EntryStore REST API. The library is targeting .Net Standard 2.0, and can be used on all major .Net platforms. Note that the library references the NuGet packages 'RestSharp' (for REST communication) and 'dotNetRDF' (for parsing RDF graphs).

Each example is a separate console application that target .Net Core 2.0, and can be run cross-platform.

**NOTE:** Projects targeting .Net Standard 2.0 & .Net Core 2.0 requires Visual Studio 2017 15.3. If you are stuck with an older version of Visual Studio, all projects need to be converted into .Net Framework projects, since no version of RestSharp targets an older version of .Net Standard than 2.0, the only alternative is to use .Net Framework.