# EntryStore examples and techniques

In this repository we give 9 basic examples of how to interact with the EntryStore REST API. The examples are implemented in several different programming languages.

## Initialization

To get the right libraries installed run:

    > yarn

Before you run the examples you have to have an EntryStore to communicate with. For the examples running in webbpages (the entrystore.js examples) you also need a webbserver. To start both Entrystore backend with metadata AND the webserver:

    > yarn start

To stop both the Entrystore backend and the webbserver:

    > yarn stop

If you run examples that change the metadata in a disruptive way, e.g. basic example 9 you can reset the EntryStore by running:

    > yarn restart

To see the examples, go to the example list at:

[http://127.0.0.1:8281/examples/](http://127.0.0.1:8281/examples/)

## Languages

 1. [HTTP/CURL](http/) - outlining the basic interactions on the protocoll level
 2. [EntryStore.js](entrystore.js/) - this is the official javascript library and the recommended approach for working with the API, works in both the browser and in Node.js.
 3. [C Sharp](csharp) - outlines how to work with clients for doing REST calls, parse JSON and work 
 with RDF in C#.
 
## Prerequisities
Before going through the examples it is useful to have a minimal understanding of what metadata and RDF is and what an `entry` in EntryStore is. Check out the [background document](BACKGROUND.md) if you feel that you need a 5 minute tutorial on these concepts.

## Basic examples

1. Search for entries with a specific title
2. Retrieve an entry with a specific identifier
3. Authenticate with username and password
4. Create an entry with metadata (RDF graph)
5. Create an entry with a given URI (a link)
6. Create an entry with a file (information resource)
7. Update metadata for an entry
8. Publish an entry (change ACL)
9. Delete an entry

## Search examples - TODO

1. Search for entries with a specific property

## Create examples - TODO

1. Create a container for entries (a context)
2. Create a user (also an entry)
3. Create a group (also an entry)
4. Create a list entry
5. Create a graph entry
6. Create string entry
3. Create an entry with cached external metadata

## Advanced examples - TODO

1. Change link for an entry
2. Changing a username
3. Uploading files in the browser environment
4. Use the metadata proxy to retrieve metadata
5. Use the echo resource to load files into the browser environment
6. Cross domain requests with JSONP and CORS support
7. Use if-un-modified-since to avoid overwriting other peoples modifications
8. Creating and using pipelines

## Usefull techniques - TODO

1. Update or create Entry with given id
2. How to create many entries
3. How to remove many entries
4. Create entries that refer to each other
