# EntryStore.js

The EntryStore.js is a javascript library that:
* covers all of EntryStore REST API
* up to date, as it is maintained together with EntryStore

Read more at [entrystore.org.](http://entrystore.org/#!JavaScript.md)

Go to the [index.html](index.html) page in this directory to see a clickable list of the examples.