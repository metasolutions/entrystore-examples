let initialized = false;
let contentNode;
const init = function() {
  if (initialized) {
    return;
  }
  initialized = true;
  const snode = document.createElement('style');
  snode.innerHTML = `
html, body {
    background: #00A9D3;
    height: 100%;
    margin: 0px;
    padding: 0px;
    overflow-y: hidden;
}
#content {
    margin: 0px auto;
    padding:20px;
    width: 800px;
    background: white;
    height: 100%;
    overflow-y: auto;
}
ul {
    padding-left: 0px;
}
li {
    list-style: none;
    padding: 10px;
    background: #8CC63F;
    margin: 5px;
}`;
  document.head.appendChild(snode);
  contentNode = document.getElementById('content');
  if (!contentNode) {
    contentNode = document.createElement('div');
    contentNode.id = 'content';
    document.body.appendChild(contentNode);
  }
};

window.init = init;

const messages = [];
export default function(message) {
  init();
  messages.push(message);
  contentNode.innerHTML = '<ul><li>'+messages.join('</li><li>')+'</li></ul>';
};